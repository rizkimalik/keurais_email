function substring_char(string, char1, char2) {
    if (Boolean(string.indexOf(char1) + 1) && Boolean(string.indexOf(char2))) {
        return string.slice(
            string.indexOf(char1) + 1,
            string.indexOf(char2),
        );
    }
    return ''
}

module.exports = substring_char;