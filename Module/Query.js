const mysql = require('mysql2');
const setting = require('./Settings');
const logger = require('./logger');
const substring_char = require('./substring_char');

const saveEmail = async function (id, interaction_id, account_email, to, from, cc, subject, date, body) {
    logger("Save.Data.Id=" + id);
    let check_subject = substring_char(subject, '[', ']');
    let get_id = check_subject.indexOf('#JKM-') === 0 ? check_subject.split('-')[1] : '';
    let interaction_identity = get_id ? get_id : interaction_id;

    const ticket_number = await getTicketNumber(interaction_identity);
    const param = [id, interaction_identity, 'IN', account_email, from, to, cc, subject, body, ticket_number, date, new Date()];
    const myQuery = `insert into email_mailbox (email_id,interaction_id,direction,account_email,efrom,eto,ecc,esubject,ebody_html,ticket_number,date_email,date_receive) values (?,?,?,?,?,?,?,?,?,?,?,?)`;

    logger(`Execute.Query=${myQuery}, Param=${param}`);

    const connection = mysql.createConnection(setting);
    const data = connection.query(myQuery, param, (err, rows) => {
        if (err) logger(`Error=${err.message}`);
        else logger(`Rows.Affected=${rows.affectedRows}`);
    });

    connection.end((error) => {
        if (error) logger("ERROR!:saveEmail.End.Database.Connection");
        else logger("saveEmail.End.Database.Connection")
    })
}

const getEmailAccountInbound = async function () {
    try {
        const myQuery = "select username,password,host,port,tls from email_account where type='Inbound' and active='1'";

        logger(`Execute.Query=${myQuery}`);

        const connection = await mysql.createConnection(setting);
        const [rows, fields] = await connection.promise().query(myQuery);

        connection.end((error) => {
            if (error) logger("ERROR!:getEmailAccountInbound.End.Database.Connection");

            logger("getEmailAccount.End.Database.Connection")
        });
        return rows;
    }
    catch (error) {
        logger("ERROR!;STATE=getEmailAccountInbound;Msg=" + error.message);
    }
}

const saveAttachment = function (id, path, filename) {
    const param = [id, path, filename];
    const myQuery = `insert into email_attachments (email_id,url,filename) values (?,?,?)`;

    logger(`Execute.Query=${myQuery}, Param=${param}`);

    const connection = mysql.createConnection(setting);
    const data = connection.query(myQuery, param, (err, rows) => {
        if (err) logger(`Error=${err.message}`);
        else logger(`Rows.Affected=${rows.affectedRows}`);
    });

    connection.end((error) => {
        if (error) logger("ERROR!:saveAttachment.End.Database.Connection");
        else logger("saveAttachment.End.Database.Connection")
    })
}

const getEmailAccount = async function (email) {
    try {
        const param = [email];
        const myQuery = `select username, password, host, port, tls from email_account where username=? and type='Outbound' and active='1' LIMIT 1`;

        logger(`Execute.Query=${myQuery}, Param=${param}`);

        const connection = await mysql.createConnection(setting);
        const [rows, fields] = await connection.promise().query(myQuery, param);

        connection.end((error) => {
            if (error) logger("ERROR!:getEmailAccount.End.Database.Connection");

            logger("getEmailAccount.End.Database.Connection")
        });
        //logger(`Username=${rows[0].username},Password=${rows[0].password}`);
        return rows[0];
    }
    catch (error) {
        logger("ERROR!;STATE=getEmailAccount;Msg=" + error.message);
    }
}

const updateEmailOutStatus = function (id, status, message) {
    const param = [id];
    const myQuery = `update email_out set sent='${status}', sent_message='${message}', date_sent=now() where id=?`;

    logger(`Execute.Query=${myQuery}, Param=${param}`);

    const connection = mysql.createConnection(setting);
    const data = connection.query(myQuery, param, (err, rows) => {
        if (err) logger(`Error=${err.message}`);
        else logger(`Rows.Affected=${rows.affectedRows}`);
    });

    connection.end((error) => {
        if (error) logger("ERROR!:updateEmailOutStatus.End.Database.Connection");
        else logger("updateEmailOutStatus.End.Database.Connection")
    })
}

const getAttachmentInbound = async function (id) {
    try {
        const param = [id];
        const myQuery = `select url,filename from email_attachments where email_id=?`;

        logger(`Execute.Query=${myQuery}, Param=${param}`);

        const connection = await mysql.createConnection(setting);
        const [rows, fields] = await connection.promise().query(myQuery, param);

        connection.end((error) => {
            if (error) logger("ERROR!:getAttachmentInbound.End.Database.Connection");

            logger("getAttachmentInbound.End.Database.Connection")
        });
        var data = [];
        for (var i = 0; i < rows.length; i++) {
            var newData = {
                filename: rows[i].filename,
                path: rows[i].url + rows[i].filename
            }
            data.push(newData);
        }
        return data;
    }
    catch (error) {
        logger("ERROR!;STATE=getEmailAccount;Msg=" + error.message);
    }
}

async function getTicketNumber(interaction_id) {
    const myQuery = `select * from email_mailbox where interaction_id='${interaction_id}' order by id asc limit 1`;
    const connection = await mysql.createConnection(setting);
    const [rows, fields] = await connection.promise().query(myQuery);
    return rows.length > 0 ? rows[0].ticket_number : '';
}

module.exports = {
    saveEmail,
    saveAttachment,
    updateEmailOutStatus,
    getEmailAccount,
    getEmailAccountInbound,
    getAttachmentInbound
}